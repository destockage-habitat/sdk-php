<?php
namespace DestockageHabitat\Sdk\Test\Resource;

use DestockageHabitat\Sdk\Resource\AbstractCollection;

class CollectionMock extends AbstractCollection
{
    protected $resourceClass = ResourceMock::class;
}
