Http Adapter
============

The Destockage-Habitat SDK comes with an adapter to use Guzzle6 Http client library.
But you can develop your own adapter if need be.

Integration
-----------
  
To use your own http client you just have to develop the proper adapter which should implement `Http\Adapter\AdapterInterface`.
Then declare it as an option you pass to the client :  

```php
$customerAdapter = new YourCustomAdapter();

$clientOptions = new \DestockageHabitat\Sdk\Client\ClientOptions();
$clientOptions->setHttpAdapter($customerAdapter);

$client  = new \DestockageHabitat\Sdk\Client\Client($clientOptions);
```

Adapter Creation
----------------

To develop your own adapter create a new class implementing `Http\Adapter\AdapterInterface` with these methods :

| Method          | Parameters                                                                                                           | Description |
|-----------------|----------------------------------------------------------------------------------------------------------------------|-------------|
| `send`          | <ul><li>`Psr\RequestInterface $request`</li><li>`array $options = []`</li></ul>                                      | Handle the http call of the given request. |
| `batchSend`     | <ul><li>`array $requests`</li><li>`array $options = []`</li></ul>                                                    | Send multiple requests |
| `createRequest` | <ul><li>`string $method`</li><li>`string $uri`</li><li>`array $headers = []`</li><li>`string $body = null`</li></ul> | Create a request from given parameters |
| `withToken`     | <ul><li>`string $token`</li><li>`$stack`</li></ul>                                                                   | Create a new instance of the adapter that use the token for identification |
| `configure`     | <ul><li>`ClientOptions $options`</li></ul>                                                                           | Allow to configure current adapter with new config object. This method should override any previous configuration set with the given one |
