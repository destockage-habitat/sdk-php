<?php
namespace Test;

use DestockageHabitat\Sdk\Client\Client;
use DestockageHabitat\Sdk\Client\ClientOptions;
use DestockageHabitat\Sdk\Credential\Password;
use DestockageHabitat\Sdk\Credential\Token;
use Exception;

session_start();

// test server
define('ENDPOINT', 'https://ws.destockage-habitat.com/api/2');
define('USERNAME', 'marcel@demo.com');
define('PASSWORD', '@Demo44');
define('URL_BACK_DESTOCKAGE', 'https://ws.destockage-habitat.com/admin/stores');

//define('ENDPOINT', 'http://www.destockage-habitat.test/api/2');
//define('URL_BACK_DESTOCKAGE', 'http://www.destockage-habitat.test/admin/stores');
//define('USERNAME', 'donald');
//define('PASSWORD', '12345678');

require('vendor/autoload.php');

function token() {
    return isset($_SESSION['token']) ? $_SESSION['token'] : null;
}

function currentUrl() {
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .
        "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function formatArray(array $arr) {
    $str = '';

    foreach ($arr as $key => $value) {
        if (is_array($value)) {
            $str .= formatArray($value);
            continue;
        }

        $str .= "$key: $value" . "<br>";
    }

    return $str;
}

class Controller  {

    public function getApi()
    {
        $credential = new Token(token());

        $options = new ClientOptions();
        $options->setBaseUri(ENDPOINT);

        try {
            $session = Client::createSession($credential, $options);
        } catch (Exception $e) {
            unset($_SESSION['token']);
            header('Location: examples.php');
            exit;
        }

        return $session->getOrderApi();
    }

    public function token()
    {
        $credential = new Password(USERNAME, PASSWORD);

        $options = new ClientOptions();
        $options->setBaseUri(ENDPOINT);
        $client = new Client($options);
        $halClient = $client->getHalClient();
        $token = $credential->geToken($halClient);

//        $session    = Client::createSession($credential);
        $_SESSION['token'] = $token->getToken();
    }

    public function accept()
    {
        $order = $_POST['order'];
        $item = $_POST['item'];

        $api = $this->getApi();
        $api->accept($order, [$item]);
    }

    public function refuse()
    {
        $order = $_POST['order'];
        $item = $_POST['item'];

        $api = $this->getApi();
        $api->refuse($order, [$item]);
    }

    public function ship()
    {
        $ref = $_POST['order'];
        $code = $_POST['tracking_code'];
        $url = $_POST['tracking_url'];

        $api = $this->getApi();

        $api->ship($ref, [], $code, $url);
    }

    public function create()
    {
        $references = $_POST['products'];
        $quantities = $_POST['quantities'];
        $api = $this->getApi();

        $items = [];

        foreach ($references as $index => $reference) {

            if (! $reference) {
                continue;
            }

            $item = new \stdClass();
            $item->product = $reference;
            $item->quantity = isset($quantities[$index]) ? $quantities[$index] : 1;

            $items[] = $item;
        }

        $api->createTest($items);
    }
}


$controller = new Controller();

if (isset($_POST['action'])) {
    $action = $_POST['action'];

    switch ($action) {
        case 'accept':
            $controller->accept();
            break;
        case 'refuse':
            $controller->refuse();
            break;
        case 'ship':
            $controller->ship();
            break;
        case 'token':
            $controller->token();
            break;
        case 'create':
            $controller->create();
            break;
        default:
            die('invalid action');
    }

    header('Location: ' . currentUrl());
    exit;
}

$filters = [];
$filters['status'] = isset($_GET['status']) ? $_GET['status'] :  null;
$filters['since'] = isset($_GET['since']) ? $_GET['since'] :  null;
$filters['until'] = isset($_GET['until']) ? $_GET['until'] :  null;

$criteria = [
    'limit' => 10,
    'page' => 1,
    'filters' => $filters
];

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <style>
        form {
            display: inline;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div>
        <label>Token: <input name="token" value="<?= isset($_SESSION['token']) ? $_SESSION['token'] : null ?>"></label>
        <form method="post">
            Demander un token pour la société test
            <input type="hidden" name="action" value="token">
            <button type="submit">Envoie</button>
        </form>
    </div>

    <?php if (isset($_SESSION['token'])): ?>

        <?php
        $api = $controller->getApi();
        $orders = $api->getPage($criteria);
        ?>
    <br><br>

    <div style="margin-bottom: 40px;">
        <h3>Créer un bon de commande de test:</h3>
        <p>Les produits disponibles dans votre magasin sont visibles sur le tableau de bord <a target="_blank" href="<?= URL_BACK_DESTOCKAGE ?>">Destockage-Habitat</a></p>
        <form method="post">

            <label>Référence produit <input name="products[]"></label>
            <label>Quantité <input type="integer" name="quantities[]"></label><br>
            <label>Référence produit <input name="products[]"></label>
            <label>Quantité <input type="integer" name="quantities[]"></label>
            <input type="hidden" name="action" value="create">
            <button type="submit">Créer</button>
        </form>
    </div>

    <div>
        <form method="get">
            <?php
            $statusList = ["SHIPPED", "WAITING_ACCEPTANCE", "WAITING_FOR_SHIPMENT", "REFUSED"]
            ?>
            <label>Statut
                <select name="status">
                    <option></option>
                    <?php foreach ($statusList as $status): ?>
                    <option <?= $status == $filters['status'] ? 'selected' : null ?>><?= $status ?></option>
                    <?php endforeach ?>
                </select>
            </label>
            <label>
                Depuis <input type="date" name="since" value="<?= $filters['since'] ?>">
            </label>
            <label>
                Jusqu'à <input type="date" name="until" value="<?= $filters['until'] ?>">
            </label>
            <input type="submit">
        </form>

    </div>
    <br><br>
    <table class="table table-bordered">
        <tr>
            <th>Référence</th>
            <th>Date de commande</th>
            <th>Statut</th>
            <th>Produits</th>
            <th>Livraisons</th>
            <th>Addresse de livraison</th>
            <th>Addresse de facturation</th>

        </tr>

        <?php foreach ($orders as $order): ?>
        <tr>
            <td><?= $order->getReference() ?><br></td>
            <td><?= $order->getCreatedAt()->format('Y/m/d H:i') ?><br></td>
            <td><?= $order->getStatus() ?><br></td>
            <td>
                <table class="table">
                    <tr>
                        <th>Produit</th>
                        <th>Quantité</th>
                        <th>Actions</th>
                    </tr>
                <?php foreach ($order->getItems() as $item): ?>
                    <tr>
                        <td><?= $item->getProp('name') ?></td>
                        <td><?= $item->getProp('quantity') ?></td>
                        <td>
                        <?php if ($item->isPending()): ?>
                        <form method="post">
                            <input type="hidden" name="order" value="<?= $order->getReference() ?>">
                            <input type="hidden" name="item" value="<?= $item->getReference() ?>">
                            <input type="hidden" name="action" value="accept">
                            <button type="submit">accept</button>
                        </form>
                        <form  method="post">
                            <input type="hidden" name="order" value="<?= $order->getReference() ?>">
                            <input type="hidden" name="item" value="<?= $item->getReference() ?>">
                            <input type="hidden" name="action" value="refuse">
                            <button type="submit">refuse</button>
                        </form>
                        <?php endif; ?>
                        </td>

                    </tr>
                <?php endforeach; ?>
                </table>
            </td>
            <td>
                <?php if ($order->isValidated()): ?>
                    Créer une livraison:
                    <form method="post">
                        <input type="hidden" name="action" value="ship">
                        <input type="hidden" name="order" value="<?= $order->getReference() ?>">
                        <label>Code de suivi<input name="tracking_code"></label>
                        <label>Url de suivi<input name="tracking_url"></label>
                        <input type="submit">
                    </form>
                <?php endif; ?>

                <?php if ($order->isShipped()): ?>
                    <?php foreach ($order->getDeliveries() as $delivery): ?>
                       <?= formatArray($delivery->toArray()) ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </td>
            <td>
                <?= formatArray($order->getShippingAddress()) ?>
            </td>
            <td>
                <?= formatArray($order->getBillingAddress()) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>
</body>
</html>


