<?php
namespace DestockageHabitat\Sdk\Api\Session;

use DestockageHabitat\Sdk\Api\Order\OrderDomain;
use DestockageHabitat\Sdk\Resource\AbstractResource;

class SessionResource extends AbstractResource
{
    /**
     * @return int|null NULL when account id is not found, id integer otherwise
     */
    public function getId()
    {
        if ($account = $this->resource->getFirstResource('account')) {
            return $account->getProperty('id');
        }

        return null;
    }

    /**
     * @return array A list of named roles
     */
    public function getRoles()
    {
        return $this->getProperty('roles') ?: [];
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return (string) $this->resource->getProperty('login');
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return (string) $this->resource->getProperty('email');
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return (string) $this->resource->getProperty('token');
    }

    /**
     * Return the language tag, as following:
     *
     * - en_US
     * - fr_FR
     *
     * ...etc
     *
     * @return string
     */
    public function getLanguageTag()
    {
        return (string) $this->resource->getProperty('language');
    }

    /**
     * @return OrderDomain
     */
    public function getOrderApi()
    {
        return new OrderDomain(
            $this->resource->getLink('order')
        );
    }
}
