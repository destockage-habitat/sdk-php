<?php
namespace DestockageHabitat\Sdk\Api\Order;

use DestockageHabitat\Sdk\Resource\AbstractDomainResource;

/**
 * @method OrderResource[] getIterator()
 * @method OrderResource[] getAll($criteria = [])
 * @method OrderResource[] getPage(array $criteria = [])
 * @method OrderResource[] getPages(array $criteria = [])
 * @method OrderResource getOne($identity)
 */
class BookingDomain extends AbstractDomainResource
{
    /**
     * @var string
     */
    protected $resourceClass = OrderResource::class;

    /**
     * @param OrderOperation $operation
     *
     * @return OrderOperationResult
     */
    public function execute(OrderOperation $operation)
    {
        return $operation->execute($this->link);
    }

    public function accept($reference, $products = [], $reason = 'ttt')
    {
        $link  = $this->link->withOperation('accept');
//        $class = $this->resourceClass;

        $data = compact("reference", "products", "reason");
        $halResource =  $link->post($data);

        $class = $this->resourceClass;

        return new $class($halResource);
    }

    public function refuse($reference, $products = [], $reason = 'ttt')
    {
        $link  = $this->link->withOperation('refuse');
//        $class = $this->resourceClass;

        $data = compact("reference", "products", "reason");
        $halResource =  $link->post($data);

        $class = $this->resourceClass;

        return new $class($halResource);
    }

    public function ship($reference, $products = [], $trackingCode = '', $trackingUrl = '')
    {
        $link  = $this->link->withOperation('ship');
//        $class = $this->resourceClass;

        $data = [
            'reference' => $reference,
            'products' => $products,
            'tracking_code' => $trackingCode,
            'tracking_url' => $trackingUrl
        ];

        $halResource =  $link->post($data);

        $class = $this->resourceClass;

        return new $class($halResource);
    }

    public function createTest(array $items)
    {
        $halResource =  $this->link->post(['items' => $items]);

        $class = $this->resourceClass;

        return new $class($halResource);
    }

}
