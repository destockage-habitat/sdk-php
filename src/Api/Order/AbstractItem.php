<?php
namespace DestockageHabitat\Sdk\Api\Order;

/**
 * Class that represents the cart items of the order.
 */
abstract class AbstractItem
{

    protected $properties;

    public function __construct($properties)
    {
        $this->properties = $properties;
    }

    public function getProp($key)
    {
        return isset($this->properties[$key]) ? $this->properties[$key] : null;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->properties;
    }
}
