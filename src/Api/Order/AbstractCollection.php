<?php
namespace DestockageHabitat\Sdk\Api\Order;

use ArrayIterator;
use Countable;
use IteratorAggregate;

abstract class AbstractCollection implements Countable, IteratorAggregate
{
    /**
     * @var AbstractItem[]
     */
    protected $items = [];

    static $resourceClass;

    public static function fromProperties(array $items)
    {
        $instance = new static;

        foreach ($items as $item) {
            $instance->add(
                new static::$resourceClass($item)
            );
        }

        return $instance;
    }

    public function count()
    {
        return count($this->items);
    }

    /**
     * @return ArrayIterator|AbstractItem[]
     */
    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_map(function(AbstractItem $item) {
            return $item->toArray();
        }, $this->items);
    }

    /**
     * @param $item
     */
    protected function add(AbstractItem $item)
    {
        $this->items[] = $item;
    }
}
