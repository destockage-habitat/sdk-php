<?php
namespace DestockageHabitat\Sdk\Api\Order;

use DateTimeImmutable;
use DestockageHabitat\Sdk\Resource\AbstractResource;
use Exception;

class OrderResource extends AbstractResource
{
    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->getProperty('id');
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return (string) $this->getProperty('reference');
    }

    public function getCustomerEmail()
    {
        return (string) $this->getProperty('customer_email');
    }

    public function getCustomerId()
    {
        return (string) $this->getProperty('customer_id');
    }

    /**
     * @return null|string
     */
    public function getStoreReference()
    {
        return $this->getProperty('storeReference');
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return (string) $this->getProperty('status');
    }

    public function isValidated()
    {
        return $this->getStatus() == 'WAITING_FOR_SHIPMENT';
    }

    public function isShipped()
    {
        return $this->getStatus() == 'SHIPPED';
    }

    /**
     * @return null|DateTimeImmutable
     * @throws Exception
     */
    public function getAcknowledgedAt()
    {
        return $this->getPropertyDatetime('acknowledgedAt');
    }

    /**
     * @return null|DateTimeImmutable
     * @throws Exception
     */
    public function getUpdatedAt()
    {
        return $this->getPropertyDatetime('updatedAt');
    }

    /**
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function getCreatedAt()
    {
        return $this->getPropertyDatetime('createdAt');
    }

    /**
     * @return array
     */
    public function getShippingAddress()
    {
        return $this->getProperty('shippingAddress');
    }

    /**
     * @return array
     */
    public function getBillingAddress()
    {
        return $this->getProperty('billingAddress');
    }

    /**
     * @return array
     */
    public function getPaymentInformation()
    {
        return $this->getProperty('payment');
    }

    /**
     * @return array
     */
    public function getShipment()
    {
        return $this->getProperty('shipment');
    }

    /**
     * Fetch order items details
     * The resource has to be loaded to access to items collection
     *
     * @return OrderItemCollection|OrderItem[]
     */
    public function getItems()
    {
        return OrderItemCollection::fromProperties(
            $this->getProperty('items', true) ?: []
        );
    }

    /**
     * @return DeliveryCollection
     */
    public function getDeliveries()
    {
        return DeliveryCollection::fromProperties(
            $this->getProperty('shipments', true) ?: []
        );
    }

}
