<?php
namespace DestockageHabitat\Sdk\Api\Order;

class OrderItemCollection extends AbstractCollection
{
    static $resourceClass = OrderItem::class;
}
