<?php
namespace DestockageHabitat\Sdk\Api\Order;

/**
 * Class that represents the cart items of the order.
 */
class OrderItem extends AbstractItem
{
    /**
     * @return string
     */
    public function getReference()
    {
        return $this->getProp('reference');
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->getProp('quantity');
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->getProp('unitPrice');
    }

    /**
     * @return float
     */
    public function getTaxAmount()
    {
        return $this->getProp('taxAmount');
    }

    /**
     * @return float|int
     */
    public function getTotalPrice()
    {
        return $this->getUnitPrice() * $this->getQuantity();
    }

    public function isPending()
    {
        return $this->getProp('status') == 'PENDING';
    }

}
