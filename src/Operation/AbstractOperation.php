<?php
namespace DestockageHabitat\Sdk\Operation;

use DestockageHabitat\Sdk\Hal;

abstract class AbstractOperation
{
    /**
     * @param Hal\HalLink $link
     *
     * @return mixed
     */
    abstract public function execute(Hal\HalLink $link);
}
