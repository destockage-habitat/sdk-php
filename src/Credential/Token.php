<?php
namespace DestockageHabitat\Sdk\Credential;

use DestockageHabitat\Sdk\Hal;
use DestockageHabitat\Sdk\Api\Session\SessionResource;

class Token implements CredentialInterface
{
    /**
     * @var string
     */
    private $token;

    /**
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = trim($token);
    }

    /**
     * @inheritdoc
     */
    public function authenticate(Hal\HalClient $client)
    {
        $client = $client->withToken($this->token);

        return new SessionResource(
            $client->request('GET', '2/me'),
            false
        );
    }

    public function getToken()
    {
        return $this->token;
    }
}
