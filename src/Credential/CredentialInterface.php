<?php
namespace DestockageHabitat\Sdk\Credential;

use DestockageHabitat\Sdk\Hal;

interface CredentialInterface
{
    /**
     * @param Hal\HalClient $client
     *
     * @return \DestockageHabitat\Sdk\Api\Session\SessionResource
     */
    public function authenticate(Hal\HalClient $client);
}
