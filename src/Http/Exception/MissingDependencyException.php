<?php
namespace DestockageHabitat\Sdk\Http\Exception;

class MissingDependencyException extends \Exception
{
}
